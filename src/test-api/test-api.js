import { LitElement, html } from "lit-element";

class TestApi extends LitElement {       

    static get properties() {
        return{
            movies: {type: Array}
        };
    }

    constructor () {
        super();

        //INICIALIZAMOS VACIO
        this.movies = [];

        //ejecutamos la funcion getMovieData
        this.getMovieData();
        
    } 

    render() {      
        return html`
            ${this.movies.map(
                //el map es la funcion para recorrer el array movies que hemos definido en las propiedades
                //movie solo existe en el ambito del map
                movie => html`<div> La pelicula ${movie.title}, fue dirigida por ${movie.director}</div>`
            )}
        `;    
    }

    getMovieData() {
        console.log("getMovieData");
        console.log("Obteniendo datos de las peliculas");

        let xhr = new XMLHttpRequest();

        //CONTESTA LA PETICION
        xhr.onload = () => {
            if (xhr.status === 200) {

                //this es el componente en el que estamos (en este caso test-api.js)
                console.log(this);

                console.log("Peticion completada correctamente");

                //vemos que no esta devolviendo la API parseado a formato JSON    
                console.log(JSON.parse(xhr.responseText));

                let APIResponse = JSON.parse(xhr.responseText);
                
                //no se puede asumir que la respuesta de la API tiene el results, va en funcion de lo que nos venga del BackEnd
                this.movies = APIResponse.results;

            } else if (xhr.status === 404) {
                console.log("No encontrado")
            }   
        }

        xhr.open("GET", "https://swapi.dev/api/films");
        xhr.send();

        console.log("Fin de getMovieData");
    }
}

customElements.define('test-api',TestApi);