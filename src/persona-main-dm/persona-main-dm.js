import { LitElement, html } from "lit-element";

class PersonaMainDm extends LitElement {       

    static get properties() {
        return{
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor () {
        super();
        
        this.people = [
            {
                name: "Ronaldo",
                yearsInCompany: 5,
                photo: {
                    src: "./img/ronaldo.jpg",
                    alt: "Ronaldo"
                },
                profile: "Delantero Killer"
            }, {
                name: "Zidane",
                yearsInCompany: 10,
                photo: {
                    src: "./img/zidane.jpeg",
                    alt: "Zidane"
                },
                profile: "Mediapunta Jugon"
            }, {
                name: "Raul",
                yearsInCompany: 15,
                photo: {
                    src: "./img/raul.jpg",
                    alt: "Raul"
                },
                profile: "Delantero Pillo"
            }, {
                name: "Benzema",
                yearsInCompany: 13,
                photo: {
                    src: "./img/benzema.jpg",
                    alt: "Benzema"
                },
                profile: "Delantero Jugon"
            }, {
                name: "Marcelo",
                yearsInCompany: 14,
                photo: {
                    src: "./img/marcelo.jpg",
                    alt: "Marcelo"
                },
                profile: "Lateral"
            }
        ]
    }    

    updated(changedProperties) {
        console.log("updated");

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people");

            this.dispatchEvent(
                new CustomEvent(
                    "people-data-updated",
                    {
                        detail: {
                            people : this.people
                        }
                    }
                )
            )
        }
    }

}

customElements.define('persona-main-dm',PersonaMainDm);